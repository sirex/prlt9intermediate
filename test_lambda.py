import operator
from functools import reduce
from typing import Callable
from typing import Dict
from typing import List


def test_lambda():
    f: Callable[[int, int], int] = lambda a, b: a + b

    def f_(a, b):
        return a + b

    assert f(2, 2) == 4
    assert f_(2, 2) == 4


def test_without_map():
    a = [1, 2, 3, 4]
    b = []
    for x in a:
        b.append(x**2)
    assert b == [1, 4, 9, 16]


def test_map():
    a = [1, 2, 3, 4]
    b = map(lambda x: x**2, a)
    assert list(b) == [1, 4, 9, 16]


def test_map_2():
    def power2(x):
        return x**2

    a = [1, 2, 3, 4]
    b = map(power2, a)
    assert list(b) == [1, 4, 9, 16]


def test_filter():
    a = [1, 2, 3, 4]
    b = filter(lambda x: x % 2 == 0, a)
    assert list(b) == [2, 4]


def test_reduce():
    a = [1, 2, 3, 4]
    b = reduce(lambda x, y: x + y, a)
    assert b == 10


def test_zip():
    a = [1, 2, 3, 4]
    b = [5, 6, 7, 8]
    c = map(lambda x: x[0] + x[1], zip(a, b))
    assert list(c) == [6, 8, 10, 12]


def test_sort():
    a: List[Dict[str, int]] = [
        {'key': 5},
        {'key': 3},
        {'key': 2},
        {'key': 6},
    ]
    b: List[Dict[str, int]] = sorted(a, key=lambda x: x['key'])
    assert list(b) == [
        {'key': 2},
        {'key': 3},
        {'key': 5},
        {'key': 6},
    ]


def test_max():
    a: List[Dict[str, int]] = [
        {'key': 5},
        {'key': 3},
        {'key': 2},
        {'key': 6},
    ]
    b = max(a, key=lambda x: x['key'])
    assert b == {'key': 6}


def test_example_1():
    def f(g: Callable[[int, int], int]):
        return g(2, 2)

    def g_(a: int, b: int) -> int:
        return a + b

    assert f(g_) == 4
    assert f(lambda a, b: a + b) == 4

