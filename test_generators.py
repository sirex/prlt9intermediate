import itertools
from pathlib import Path
from typing import Any
from typing import Callable
from typing import Dict
from typing import Generator
from typing import IO
from typing import Iterable
from typing import Iterator
from typing import List
from typing import Tuple
from typing import TypeVar

import pytest


def test_what_is_iterable():
    a: Iterable[int] = [1, 2, 3, 4]
    #                  {1, 2, 3, 4}
    #                  (1, 2, 3, 4)
    #                  {1: 'a', 2: 'b', 3: 'c', 4: 'd')
    # {1, 2, 3, 4}
    b: List[int] = []
    for x in a:
        b.append(x)
    assert a == b


def test_what_is_iterable_2():
    a: Iterable[str] = '1234'
    b: List[int] = []
    for x in a:
        b.append(int(x))
    assert b == [1, 2, 3, 4]


def test_what_is_iterator():
    a: Iterable[int] = [1, 2, 3]    # 1. Iterable
    a_it: Iterator[int] = iter(a)   # 2. Iterator

    x = next(a_it)
    assert x == 1

    x = next(a_it)
    assert x == 2

    x = next(a_it)
    assert x == 3

    with pytest.raises(StopIteration):
        next(a_it)


def test_what_is_generator():
    def g() -> Iterator[int]:
        yield 1
        yield 2
        yield 3
        yield 4

    assert isinstance(g(), Generator)

    b = []
    for x in g():
        b.append(x)

    assert b == [1, 2, 3, 4]


def test_range():
    b = []

    for i in range(10_000_000):
        if i > 2:
            break
        else:
            b.append(i)

    assert b == [0, 1, 2]


def test_infinite_counter():

    def counter():
        i = 0
        while True:
            yield i
            i += 1

    c = counter()
    assert list(itertools.islice(c, 3)) == [0, 1, 2]
    assert list(itertools.islice(c, 3)) == [3, 4, 5]


def test_what_is_possible():
    a = [1, 2, 3]
    g = iter(a)

    assert a[1] == 2

    with pytest.raises(TypeError):
        g[1]


def even(numbers: Iterable[int]):
    for n in numbers:
        if n % 2 == 0:
            yield n


def test_exercise_1():
    g = even(range(1, 9))
    assert isinstance(g, Generator)
    assert list(itertools.islice(g, 4)) == [2, 4, 6, 8]


def powers(base: int, amount: int) -> Iterator[int]:
    for i in range(amount):
        yield base**i


def test_exercise_2():
    """
    Write a generator function `powers`, that accepts two arguments, a base
    number and amount of numbers. Function should generate all powers of a given
    base up to given number in the second argument.
    """
    g = powers(2, 9)
    assert isinstance(g, Generator)
    assert list(g) == [1, 2, 4, 8, 16, 32, 64, 128, 256]


def digits(chars: Iterable[str]) -> Iterator[int]:
    for c in chars:
        if c.isdigit():
            yield int(c)


def test_exercise_3():
    """
    Create a generator function, which would read a string and would pick only
    digits, those digits should be converted to int and returned back.
    Also reuse previously written `even` generator to pick only even numbers.
    """
    g = even(digits('a2k423k56'))
    assert sum(g) == 14


def gen() -> Generator[int, str, bool]:
    a: str = yield 1
    return True


def test_true_generators():
    g: Generator[int, str, bool] = gen()
    x: int = next(g)
    g.send('1')
    r: bool = yield from g


def test_iterators_recap():
    a: Iterable = [1, 2, 3]
    b: Iterator = iter(a)

    # We can access items by index
    assert a[0] == 1

    # We can't access items by index on Iterator
    with pytest.raises(TypeError) as e:
        b[0]

    # With Iterator objects, we can only access next item
    assert next(b) == 1


def test_iterator_examples():
    d: Dict[str, int] = {'a': 1, 'b': 2}

    keys = d.keys()

    with pytest.raises(TypeError):
        keys[0]

    with pytest.raises(TypeError):
        next(keys)

    keys_it = iter(keys)
    assert next(keys_it) == 'a'

    assert list(keys) == ['a', 'b']


def test_iterator_examples_2():
    a: Iterable[str] = 'abcd'
    b: Iterator[str] = map(str.upper, a)
    assert list(b) == ['A', 'B', 'C', 'D']


def test_iterator_examples_3():
    I = TypeVar('I')    # Same as Any, but I kind of Any.
    O = TypeVar('O')    # Same as Any, but O kind of Any.

    def upper(c: str) -> str:
        return c.upper()

    def map_(
        f: Callable[[I], O],
        it: Iterable[I],
    ) -> Iterator[O]:
        for x in it:
            yield f(x)

    a: Iterable[str] = 'abcd'
    b: Iterator[str] = map_(upper, a)
    assert list(b) == ['A', 'B', 'C', 'D']


def test_iterator_data_processing(tmpdir: Path):
    data_file_path = tmpdir / 'data.txt'
    with open(data_file_path, 'w') as f:
        for c in 'abc':
            print(c, file=f)

    def convert_to_upper(f_read_: Iterator[str], f_write_: IO[str]) -> None:
        for line in f_read_:
            f_write_.write(line.upper())

    # Convert data.txt to upper case letter and write result to data_upper.txt
    data_upper_file_path = tmpdir / 'data_upper.txt'
    with open(data_file_path) as f_read, \
         open(data_upper_file_path, 'w') as f_write:
        convert_to_upper(f_read, f_write)

    # Check result.
    with open(data_upper_file_path) as f:
        lines = list(map(str.rstrip, f))
    assert lines == ['A', 'B', 'C']


def test_iterator_data_processing_2(tmpdir: Path):
    data_file_path = tmpdir / 'data.txt'
    with open(data_file_path, 'w') as f:
        for c in 'abc':
            print(c, file=f)

    def read_lines(f_: IO[str]) -> Iterator[str]:
        for line in f_:
            yield line

    def write_lines(lines: Iterator[str], f_: IO[str]) -> None:
        for line in lines:
            f_.write(line)

    # Convert data.txt to upper case letter and write result to data_upper.txt
    data_upper_file_path = tmpdir / 'data_upper.txt'
    with open(data_file_path) as f_read, \
         open(data_upper_file_path, 'w') as f_write:
        data = read_lines(f_read)
        data = map(str.upper, data)
        write_lines(data, f_write)

    # Check result.
    with open(data_upper_file_path) as f:
        result = list(map(str.rstrip, f))
    assert result == ['A', 'B', 'C']
