import datetime
import time
from multiprocessing import Process
from timeit import timeit
from concurrent.futures import ThreadPoolExecutor
from threading import Thread
from typing import List
from typing import Optional


def test_thread():
    Result = List[Optional[datetime.datetime]]

    def f(t: int, result_: Result, i: int) -> None:
        time.sleep(t)
        result_[i] = datetime.datetime.now()

    start = datetime.datetime.now()
    result: Result = [None, None, None, None]

    t1 = Thread(target=f, args=(2, result, 1), name='t1')
    t2 = Thread(target=f, args=(3, result, 2), name='t2')

    t1.start()
    t2.start()

    result[0] = datetime.datetime.now()

    t1.join()
    t2.join()

    result[-1] = datetime.datetime.now()

    result_times = [
        int(round((x - start).total_seconds(), 0))
        for x in result
    ]

    assert result_times == [0, 2, 3, 3]


def test_thread_pool():
    def f(a, b):
        time.sleep(1)
        return a + b

    with ThreadPoolExecutor(10) as pool:
        future = pool.submit(f, 2, 2)
        result = future.result()

    assert result == 4


def test_downloader():
    def download(url: str) -> str:
        time.sleep(1)
        return url

    def downloader(urls_: List[str]) -> List[str]:
        return [
            download(url)
            for url in urls_
        ]

    def downloader_with_threads(urls_: List[str]) -> List[str]:
        with ThreadPoolExecutor(10) as pool:
            tasks = [
                pool.submit(download, url)
                for url in urls_
            ]

            return [
                future.result()
                for future in tasks
            ]

    urls = [
        'https://example.com/file0.zip',
        'https://example.com/file1.zip',
        'https://example.com/file2.zip',
        'https://example.com/file3.zip',
        'https://example.com/file4.zip',
        'https://example.com/file5.zip',
        'https://example.com/file6.zip',
        'https://example.com/file7.zip',
        'https://example.com/file8.zip',
        'https://example.com/file9.zip',
    ]

    def measure(downloader_, urls_):
        globals_ = {
            'downloader': downloader_,
            'urls': urls_,
        }
        delay = timeit('downloader(urls)', number=1, globals=globals_)
        return int(delay)

    assert measure(downloader_with_threads, urls) == 1
    assert measure(downloader, urls) == 10


def test_timeit():
    def len_(arr: list):
        i = 0
        for _ in arr:
            i += 1
        return i

    t1 = timeit('len_([1, 2, 3, 4, 5])', globals=locals())
    t2 = timeit('len([1, 2, 3, 4 ,5])')
    assert t1 > t2
    print(f'{t1=}, {t2=}')


def f_(t: int) -> None:
    time.sleep(t)


def test_process():
    result: List[float] = []
    start = datetime.datetime.now()

    p1 = Process(target=f_, args=(2,))
    p2 = Process(target=f_, args=(3,))

    p1.start()
    p2.start()
    result.append((datetime.datetime.now() - start).total_seconds())

    p1.join()
    result.append((datetime.datetime.now() - start).total_seconds())

    p2.join()
    result.append((datetime.datetime.now() - start).total_seconds())

    times = list(map(int, result))
    assert times == [0, 2, 3]


def test_task_1():

    def sleep_sort(arr: List[int]) -> List[int]:
        def sort(n: int, result_: List[int]):
            time.sleep(n * .1)
            result_.append(n)

        result: List[int] = []
        threads: List[Thread] = []

        for x in arr:
            t = Thread(target=sort, args=(x, result))
            threads.append(t)
            t.start()

        for t in threads:
            t.join()

        return result

    assert sleep_sort([3, 1, 2]) == [1, 2, 3]


def test_task_1a():

    def sleep_reverse_sort(arr: List[int]) -> List[int]:
        def sort(n: int, result_: List[int]):
            time.sleep(n * .1)
            result_.insert(0, n)

        result: List[int] = []
        threads: List[Thread] = []

        for x in arr:
            t = Thread(target=sort, args=(x, result))
            threads.append(t)
            t.start()

        for t in threads:
            t.join()

        return result

    assert sleep_reverse_sort([3, 1, 2]) == [3, 2, 1]
