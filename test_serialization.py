import dataclasses
import pickle   # Skipping because we don't have enough time
import json
import csv
from enum import Enum
from pathlib import Path
from typing import Union
from typing import Any
from typing import Dict
from typing import Iterator
from typing import List

import requests


class Role(Enum):
    FATHER = 'father'
    MOTHER = 'mother'
    CHILD = 'child'


class Gender(Enum):
    MALE = 1
    FEMALE = 2


@dataclasses.dataclass()
class Person:
    name: str
    gender: Gender


@dataclasses.dataclass()
class Family:
    father: Person
    mother: Person
    children: List[Person]


def test_json(tmpdir: Path):
    people = [
        {'name': "Jonas", 'gender': "male"},
        {'name': "Marija", 'gender': "female"},
        {'name': "Juozas", 'gender': "male"},
    ]

    # Write data to JSON file
    with open(tmpdir / 'people.json', 'w', encoding='utf-8') as f:
        json.dump(people, f)

    # Read data back from JSON file
    with open(tmpdir / 'people.json', 'r', encoding='utf-8') as f:
        people_ = json.load(f)

    assert people_ == people


def test_csv(tmpdir: Path):
    people: List[Dict[str, str]] = [
        {'name': "Jonas", 'gender': "male"},
        {'name': "Marija", 'gender': "female"},
        {'name': "Juozas", 'gender': "male"},
    ]

    # Write data to a CSV file
    with open(tmpdir / 'people.csv', 'w', encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow(['name', 'gender'])
        for person in people:
            writer.writerow([
                person['name'],
                person['gender'],
            ])

    # Read data back from a CSV file
    with open(tmpdir / 'people.csv', 'r', encoding='utf-8') as f:
        people_: List[Dict[str, str]] = []
        reader: Iterator[List[str]] = csv.reader(f)
        next(reader)
        for row in reader:
            people_.append({
                'name': row[0],
                'gender': row[1],
            })

    assert people_ == people


def test_csv_dict_reader(tmpdir: Path):
    people: List[Dict[str, str]] = [
        {'name': "Jonas", 'gender': "male"},
        {'name': "Marija", 'gender': "female"},
        {'name': "Juozas", 'gender': "male"},
    ]

    # Write data to a CSV file
    with open(tmpdir / 'people.csv', 'w', encoding='utf-8') as f:
        writer = csv.DictWriter(f, ['name', 'gender'])
        writer.writeheader()
        writer.writerows(people)

    # Read data back from a CSV file
    with open(tmpdir / 'people.csv', 'r', encoding='utf-8') as f:
        reader: Iterator[Dict[str, Any]] = csv.DictReader(f)
        people_ = list(reader)

    assert people_ == people


def test_csv_laimonas(tmpdir: Path):
    with open(tmpdir / 'people.csv', 'w', encoding='utf-8') as f:
        print('name,gender', file=f)
        print('foo bar baz,male', file=f)

    with open(tmpdir / 'people.csv', 'r', encoding='utf-8') as f:
        reader: Iterator[Dict[str, Any]] = csv.DictReader(f)
        row = next(reader)
        data = row['name'].split()[:2]

    assert data == ['foo', 'bar']


def get_avg(numbers: List[Any]) -> float:
    numbers = [x for x in numbers if isinstance(x, (int, float))]
    return sum(numbers) / len(numbers)


def test_task_1(tmpdir: Path):
    data_file = (tmpdir / 'data.json')
    data_file.write_text('''\
    {
        "right_side": [
            3,
            5,
            3,
            6,
            4,
            2,
            3,
            6,
            8,
            4,
            3,
            2
        ],
        "left_side": [
            1.2,
            4.3,
            5.4,
            6.9,
            9.9,
            7.2
        ]
    }
    ''', encoding='utf-8')

    with open(data_file, 'r') as f:
        data = json.load(f)

    right_side_avg = get_avg(data['right_side'])
    left_side_avg = get_avg(data['left_side'])
    both_sides_avg = get_avg([right_side_avg, left_side_avg])
    both_sides_avg_2 = get_avg(data['right_side'] + data['left_side'])

    assert round(right_side_avg, 2) == 4.08
    assert round(left_side_avg, 2) == 5.82
    assert both_sides_avg == 4.95
    assert round(both_sides_avg_2, 2) == 4.66


def test_task_1a():
    assert get_avg([1, 2, 'a']) == 1.5


def test_json_dumps_array():
    data: List[int] = [1, 2, 3]
    json_data: str = json.dumps(data)
    assert json_data == '[1, 2, 3]'


def test_json_loads_array():
    json_data = '[1, 2, 3]'
    data: List[int] = json.loads(json_data)
    assert data == [1, 2, 3]


def test_json_dumps_dict():
    data: Dict[str, int] = {'a': 1, 'b': 2, 'c': 3}
    json_data: str = json.dumps(data)
    assert json_data == '{"a": 1, "b": 2, "c": 3}'


def test_json_dump_array(tmpdir: Path):
    data: List[int] = [1, 2, 3]
    with open(tmpdir / 'data.json', 'w') as f:
        json.dump(data, f)

    assert (tmpdir / 'data.json').read_text(encoding='utf-8') == (
        '[1, 2, 3]'
    )


def test_json_load_array(tmpdir: Path):
    # tmpdir = Path(r'C:\User\Desktop')
    json_file_path: Path = tmpdir / 'data.json'
    json_file_path.write_text('[1, 2, 3]', encoding='utf-8')

    with open(json_file_path) as f:
        data: List[int] = json.load(f)

    total: int = sum(data)
    assert total == 6


def test_json_append_array(tmpdir: Path):
    # tmpdir = Path(r'C:\User\Desktop')
    json_file_path: Path = tmpdir / 'data.json'
    json_file_path.write_text('[1, 2, 3]', encoding='utf-8')

    # Read JSON data into data variable
    with open(json_file_path) as f:
        data: List[int] = json.load(f)

    total: int = sum(data)
    assert total == 6

    # Append new data
    data.append(4)

    # Write data back to JSON
    with open(json_file_path, 'w') as f:
        json.dump(data, f)

    total: int = sum(data)
    assert total == 10


def test_external_json_file():
    data_file_url = (
        'https://github.com/'
        'cwacek/python-jsonschema-objects/'
        'raw/master/test/thing-one.json'
    )
    resp = requests.get(data_file_url)
    resp.raise_for_status()

    data = json.loads(resp.text)
    assert data['title'] == 'thing_one'
