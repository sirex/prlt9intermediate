from __future__ import annotations

from enum import Enum
from enum import IntEnum
from enum import auto
from random import randrange
from typing import Dict
from typing import Iterator
from typing import List
from typing import NamedTuple
from typing import Optional
from typing import Tuple
from typing import TypeVar

T = TypeVar('T')


class State(IntEnum):
    empty = 0
    moving = 1
    placed = 2


class Position(NamedTuple):
    x: int
    y: int


class Matrix:
    data: List[List[State]]
    width: int
    height: int

    @classmethod
    def from_string(cls, block: str) -> Matrix:
        return cls([
            [COLORS_[col] for col in row]
            for row in block.splitlines()
            if row
        ])

    @classmethod
    def create(cls, width: int, height: int) -> Matrix:
        return cls([
            [State.empty for _ in range(width)]
            for _ in range(height)
        ])

    def __init__(self, data: List[List[State]]):
        self.data = data
        self.width = len(data[0])
        self.height = len(data)

    def fits(self, pos: Position, block: Matrix) -> bool:
        if pos.x < 0 or pos.x + block.width > self.width:
            return False
        if pos.y + block.height > self.height:
            return False
        return True

    def collides(self, pos: Position, block: Matrix) -> bool:
        if pos.y + block.height > self.height:
            return True
        for y in range(block.height):
            for x in range(block.width):
                if (
                    block.data[y][x] == State.moving and
                    self.data[pos.y + y][pos.x + x] == State.placed
                ):
                    return True
        return False

    def replace(self, pos: Position, block: Matrix, state: State) -> None:
        for y in range(block.height):
            for x in range(block.width):
                if block.data[y][x] != State.empty:
                    self.data[pos.y + y][pos.x + x] = state

    def rotate(self) -> Matrix:
        return Matrix([
            [self.data[y][x] for y in range(self.height)]
            for x in range(self.width - 1, -1, -1)
        ])

    def translate(self, table: Dict[int, T]) -> List[List[T]]:
        return [
            [table[col] for col in row]
            for row in self.data
        ]


COLORS: Dict[State, str] = {
    State.empty: ' ',
    State.moving: '#',
    State.placed: '@',
}

COLORS_: Dict[str, State] = {
    v: k
    for k, v in COLORS.items()
}

BLOCKS: List[Matrix] = list(map(Matrix.from_string, [
    '##\n'
    '##\n',

    '###\n'
    ' # \n',

    '## \n'
    ' ##\n',

    ' ##\n'
    '## \n',

    '#  \n'
    '###\n',

    '  #\n'
    '###\n',

    '####\n',
]))


def next_block(block: Optional[int] = None) -> Matrix:
    if block is None:
        block = randrange(len(BLOCKS))
    return BLOCKS[block]


def render(board: Matrix):
    rows = map(''.join, board.translate(COLORS))
    rows = map('|{}|'.format, rows)
    bottom = '-' * board.width
    return '\n'.join(rows) + f'\n+{bottom}+\n'


class Move:
    board: Matrix
    block: Optional[Matrix] = None
    pos: Optional[Position] = None

    def __init__(self, board: Matrix):
        self.board = board

    def add(self, block: Matrix, pos: Optional[Position] = None) -> bool:
        if pos is None:
            pos = Position(randrange(self.board.width), 0)
        if self.board.collides(pos, block):
            return False
        else:
            self.block = block
            self.update(pos)
            return True

    def update(self, pos: Position, state: State = State.moving) -> None:
        if self.pos is not None:
            self.board.replace(self.pos, self.block, State.empty)
        self.board.replace(pos, self.block, state)
        if state == State.placed:
            self.block = None
            self.pos = None
        else:
            self.pos = pos

    def align(self, n: int) -> None:
        pos = Position(self.pos.x + n, self.pos.y)
        if (
            self.board.fits(pos, self.block) and
            not self.board.collides(pos, self.block)
        ):
            self.update(pos)

    def rotate(self):
        block = self.block.rotate()
        if not self.board.collides(self.pos, block):
            self.board.replace(self.pos, self.block, State.empty)
            self.block = block
            self.update(self.pos)

    def down(self, n: int) -> None:
        pos = self.pos
        collides = False
        for y in range(self.pos.y + 1, self.pos.y + n + 1):
            _pos = Position(self.pos.x, y)
            if self.board.collides(_pos, self.block):
                collides = True
                break
            pos = _pos
        if collides:
            self.update(pos, State.placed)
        elif pos.y > self.pos.y:
            self.update(pos)


class Event(Enum):
    left = auto()
    right = auto()
    rotate = auto()
    down = auto()
    quit = auto()
    tick = auto()


def tetris(
    width: int,
    height: int,
    events: Iterator[Event],
    blocks: Iterator[Tuple[int, Position]],
) -> Matrix:
    board = Matrix.create(width, height)
    move = Move(board)
    while True:
        event = next(events)
        if event == Event.tick:
            if move.block is None:
                block, pos = next(blocks)
                if not move.add(BLOCKS[block], pos):
                    break
            else:
                move.down(1)
        elif event == Event.left:
            move.align(-1)
        elif event == Event.right:
            move.align(1)
        elif event == Event.rotate:
            move.rotate()
        elif event == Event.down:
            move.down(board.height)
        elif event == Event.quit:
            break
    return board
