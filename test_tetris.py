from tetris import BLOCKS
from tetris import Event
from tetris import Matrix
from tetris import Move
from tetris import Position
from tetris import render
from tetris import tetris


def test_board():
    board = Matrix.create(3, 2)
    assert render(board) == (
        '|   |\n'
        '|   |\n'
        '+---+\n'
    )


def test_move_add():
    board = Matrix.create(4, 3)
    move = Move(board)
    move.add(BLOCKS[0], Position(1, 0))
    assert render(board) == (
        '| ## |\n'
        '| ## |\n'
        '|    |\n'
        '+----+\n'
    )


def test_move_left():
    board = Matrix.create(4, 3)
    move = Move(board)
    move.add(BLOCKS[0], Position(1, 0))
    move.align(-1)
    assert render(board) == (
        '|##  |\n'
        '|##  |\n'
        '|    |\n'
        '+----+\n'
    )


def test_move_right():
    board = Matrix.create(4, 3)
    move = Move(board)
    move.add(BLOCKS[0], Position(1, 0))
    move.align(1)
    assert render(board) == (
        '|  ##|\n'
        '|  ##|\n'
        '|    |\n'
        '+----+\n'
    )


def test_move_left_boundary():
    board = Matrix.create(4, 3)
    move = Move(board)
    move.add(BLOCKS[0], Position(0, 0))
    move.align(-1)
    assert render(board) == (
        '|##  |\n'
        '|##  |\n'
        '|    |\n'
        '+----+\n'
    )


def test_move_right_boundary():
    board = Matrix.create(4, 3)
    move = Move(board)
    move.add(BLOCKS[0], Position(2, 0))
    move.align(1)
    assert render(board) == (
        '|  ##|\n'
        '|  ##|\n'
        '|    |\n'
        '+----+\n'
    )


def test_move_down():
    board = Matrix.create(4, 4)
    move = Move(board)
    move.add(BLOCKS[0], Position(1, 0))
    move.down(1)
    assert render(board) == (
        '|    |\n'
        '| ## |\n'
        '| ## |\n'
        '|    |\n'
        '+----+\n'
    )


def test_move_down_multiple():
    board = Matrix.create(4, 5)
    move = Move(board)
    move.add(BLOCKS[0], Position(1, 0))
    move.down(3)
    assert render(board) == (
        '|    |\n'
        '|    |\n'
        '|    |\n'
        '| ## |\n'
        '| ## |\n'
        '+----+\n'
    )


def test_move_collision():
    board = Matrix.create(4, 3)
    move = Move(board)
    move.add(BLOCKS[0], Position(1, 1))
    move.down(1)
    assert render(board) == (
        '|    |\n'
        '| @@ |\n'
        '| @@ |\n'
        '+----+\n'
    )


def test_move_collision_multiple():
    board = Matrix.create(4, 3)
    move = Move(board)
    move.add(BLOCKS[0], Position(1, 1))
    move.down(3)
    assert render(board) == (
        '|    |\n'
        '| @@ |\n'
        '| @@ |\n'
        '+----+\n'
    )


def test_move_down_with_block():
    board = Matrix.create(4, 5)
    move = Move(board)
    move.add(BLOCKS[0], Position(1, 0))
    move.down(4)
    move.add(BLOCKS[0], Position(2, 0))
    move.down(1)
    assert render(board) == (
        '|    |\n'
        '|  ##|\n'
        '|  ##|\n'
        '| @@ |\n'
        '| @@ |\n'
        '+----+\n'
    )


def test_move_collision_with_block():
    board = Matrix.create(4, 4)
    move = Move(board)
    move.add(BLOCKS[0], Position(1, 0))
    move.down(4)
    move.add(BLOCKS[0], Position(2, 0))
    move.down(1)
    assert render(board) == (
        '|  @@|\n'
        '|  @@|\n'
        '| @@ |\n'
        '| @@ |\n'
        '+----+\n'
    )


def test_move_add_collision():
    board = Matrix.from_string(
        '    \n'
        ' @@ \n'
        ' @@ \n'
    )
    move = Move(board)
    assert move.add(BLOCKS[0], Position(2, 0)) is False


def test_move_add_no_collision():
    board = Matrix.from_string(
        '    \n'
        '    \n'
        ' @@ \n'
        ' @@ \n'
    )
    move = Move(board)
    assert move.add(BLOCKS[0], Position(2, 0)) is True


def test_rotate_1():
    block = BLOCKS[1].rotate()
    assert render(block) == (
        '|# |\n'
        '|##|\n'
        '|# |\n'
        '+--+\n'
    )


def test_rotate_2():
    block = BLOCKS[1].rotate().rotate()
    assert render(block) == (
        '| # |\n'
        '|###|\n'
        '+---+\n'
    )


def test_rotate_3():
    block = BLOCKS[1].rotate().rotate().rotate()
    assert render(block) == (
        '| #|\n'
        '|##|\n'
        '| #|\n'
        '+--+\n'
    )


def test_rotate_4():
    block = BLOCKS[1].rotate().rotate().rotate().rotate()
    assert render(block) == (
        '|###|\n'
        '| # |\n'
        '+---+\n'
    )


def test_tetris_1():
    blocks = iter([
        (0, Position(1, 0)),
    ])
    events = iter([
        Event.tick,
        Event.quit,
    ])
    assert render(tetris(4, 3, events, blocks)) == (
        '| ## |\n'
        '| ## |\n'
        '|    |\n'
        '+----+\n'
    )


def test_tetris_2():
    blocks = iter([
        (0, Position(1, 0)),
    ])
    events = iter([
        Event.tick,
        Event.tick,
        Event.quit,
    ])
    assert render(tetris(4, 3, events, blocks)) == (
        '|    |\n'
        '| ## |\n'
        '| ## |\n'
        '+----+\n'
    )


def test_tetris_3():
    blocks = iter([
        (0, Position(1, 0)),
    ])
    events = iter([
        Event.tick,
        Event.tick,
        Event.tick,
        Event.quit,
    ])
    assert render(tetris(4, 3, events, blocks)) == (
        '|    |\n'
        '| @@ |\n'
        '| @@ |\n'
        '+----+\n'
    )


def test_tetris_4():
    blocks = iter([
        (0, Position(1, 0)),
        (5, Position(1, 0)),
    ])
    events = iter([
        Event.tick,     # block #0 start
        Event.tick,
        Event.tick,
        Event.tick,
        Event.tick,
        Event.tick,     # block #1 start
        Event.quit,
    ])
    assert render(tetris(4, 5, events, blocks)) == (
        '|   #|\n'
        '| ###|\n'
        '|    |\n'
        '| @@ |\n'
        '| @@ |\n'
        '+----+\n'
    )


def test_tetris_5():
    blocks = iter([
        (0, Position(1, 0)),
        (5, Position(1, 0)),
    ])
    events = iter([
        Event.tick,     # block #0 start
        Event.tick,
        Event.tick,
        Event.tick,
        Event.tick,
        Event.tick,     # block #1 start
        Event.tick,
        Event.quit,
    ])
    assert render(tetris(4, 5, events, blocks)) == (
        '|    |\n'
        '|   #|\n'
        '| ###|\n'
        '| @@ |\n'
        '| @@ |\n'
        '+----+\n'
    )


def test_tetris_6():
    blocks = iter([
        (0, Position(1, 0)),
        (5, Position(1, 0)),
    ])
    events = iter([
        Event.tick,     # block #0 start
        Event.down,
        Event.tick,
        Event.tick,     # block #1 start
        Event.tick,
        Event.quit,
    ])
    assert render(tetris(4, 5, events, blocks)) == (
        '|    |\n'
        '|   @|\n'
        '| @@@|\n'
        '| @@ |\n'
        '| @@ |\n'
        '+----+\n'
    )


def test_tetris_7():
    blocks = iter([
        (0, Position(1, 0)),
        (5, Position(1, 0)),
        (1, Position(1, 0)),
        (0, Position(1, 0)),
    ])
    events = iter([
        Event.tick,     # block #0 start
        Event.down,
        Event.tick,
        Event.tick,     # block #1 start
        Event.tick,
        Event.tick,     # block #2 start
        Event.tick,
        Event.tick,
    ])
    assert render(tetris(4, 5, events, blocks)) == (
        '| @@@|\n'
        '|  @@|\n'
        '| @@@|\n'
        '| @@ |\n'
        '| @@ |\n'
        '+----+\n'
    )


def test_tetris_8():
    blocks = iter([
        (0, Position(1, 0)),
        (5, Position(1, 0)),
        (1, Position(1, 0)),
        (0, Position(1, 0)),
    ])
    events = iter([
        Event.tick,     # block #0 start
        Event.down,
        Event.tick,
        Event.tick,     # block #1 start
        Event.tick,
        Event.tick,     # block #2 start
        Event.left,
        Event.rotate,
        Event.tick,
        Event.tick,
    ])
    assert render(tetris(4, 5, events, blocks)) == (
        '|@   |\n'
        '|@@ @|\n'
        '|@@@@|\n'
        '| @@ |\n'
        '| @@ |\n'
        '+----+\n'
    )
