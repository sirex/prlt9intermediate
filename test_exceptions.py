import math
import random
from typing import Dict
from typing import List
from typing import Optional

import pytest


def f1():
    return f2()


def f2():
    return f3()


def f3():
    ans = 42 / 0
    return ans


def test():
    with pytest.raises(ZeroDivisionError):
        assert f1() == 42


def test_name_error():
    with pytest.raises(NameError):
        undefined


def test_func_name_error():
    with pytest.raises(NameError):
        undefined()


def test_attr_error():
    x: int = 42
    with pytest.raises(AttributeError):
        x.append(2)


def test_manual_exception():
    with pytest.raises(Exception):
        raise Exception()


def test_fixme_1():
    a = 2
    b = 2
    assert a + b == 4


def test_fixme_2():
    a = [1, 2, 3]
    a.append(4)
    assert a == [1, 2, 3, 4]


def test_fixme_3():
    assert math.sqrt(4) == 2


def test_fixme_4():
    a = 2
    b = 2
    assert a + b == 4


def test_fixme_5():
    a = 2
    b = 2
    assert a + b == 4


class TooMuch(Exception):
    pass


def too_much(n: int) -> None:
    if n >= 40:
        raise TooMuch()


def test_fixme_6():
    with pytest.raises(TooMuch):
        too_much(40)


def fixme7():
    return fixme7_1()


def fixme7_1():
    return True


def fixme7_2():
    return False


def test_fixme_7():
    assert fixme7()


class NotFound(Exception):
    pass


MEMBERS: Dict[int, str] = {
    1: 'a',
    2: 'b',
}


def check_member(member: int) -> str:
    if member in MEMBERS:
        return MEMBERS[member]
    else:
        raise NotFound()


def test_fixme_8():
    with pytest.raises(NotFound):
        check_member(42)


def dalyba(a: int, b: int) -> Optional[float]:
    if b == 0:
        return None
    else:
        return a / b


def test_fixme_9():
    assert dalyba(2, 2) == 1
    assert dalyba(2, 0) is None


def pridek(elem: int, arr: List[int] = None) -> List[int]:
    if arr is None:
        arr = []
    arr.append(elem)
    return arr


def test_fixme_10():
    assert pridek(42) == [42]
    assert pridek(24) == [24]


def vidurkis(*skaiciai: int) -> float:
    if not skaiciai:
        raise ValueError("Skaičių masyvas yra tuščias.")
    suma = 0
    for sk in skaiciai:
        suma += sk
    return suma / len(skaiciai)


def test_fixme_11():
    assert vidurkis(4, 8) == 6


def test_fixme_12():
    with pytest.raises(ValueError):
        vidurkis()


def error(e: Optional[Exception]):
    if e is not None:
        raise e


def test_do_not_do_this_ever():
    try:
        error(Exception("klaida"))
    except:
        pass   # do not do this ever!


def test_always_handle_errors():
    with pytest.raises(Exception):
        try:
            error(Exception("klaida"))
        except:
            raise   # reraising error


def test_always_be_specific():
    with pytest.raises(Exception):
        try:
            error(Exception("klaida"))
        except ValueError:  # <- handle only specific errors
            raise


def test_always_be_specific_2():
    with pytest.raises(Exception):
        try:
            error(Exception("klaida"))
        except ValueError:  # <- handle only specific errors
            raise
        except KeyError:    # <- multiple catches
            raise


def test_always_be_specific_3():
    e = None

    try:
        error(KeyError("klaida"))
    except ValueError:  # <- handle only specific errors
        e = 'value'
    except KeyError:    # <- multiple catches
        e = 'key'

    assert e == 'key'


def test_try_else():
    was_error = True

    try:
        error(None)
    except ValueError:  # <- handle only specific errors
        raise
    except KeyError:    # <- multiple catches
        raise
    else:
        was_error = False

    assert was_error is False


def test_try_finally():
    i = 0

    # attempt 1
    try:
        error(None)
    finally:
        i += 1

    # attempt 2
    with pytest.raises(Exception):
        try:
            error(Exception("error"))
        finally:
            i += 1

    assert i == 2


def catch_errors_1(a: int, b: int):
    return a / b


def test_catch_errors_1():
    """
    Make sure, that result is set to None if catch_errors_1 raises division
    from zero. You should update the test, not the catch_errors_1 function.
    """
    try:
        result = catch_errors_1(2, 0)
    except ZeroDivisionError:
        result = None
    assert result is None


def catch_errors_2(i: int = None):
    data = [1, 2]
    if i is None:
        i = random.choice([0, 1, 2, 3])
    return data[i]


@pytest.mark.parametrize('arg,ans', [
    (0, 1),
    (1, 2),
    (2, None),
    (3, None),
])
def test_catch_errors_2(arg: int, ans: Optional[int]):
    """
    Make sure, that result is set to None if index error is raised.
    """
    try:
        result = catch_errors_2(arg)
    except IndexError:
        result = None
    assert result == ans


class CatchError3(Exception):
    pass


def catch_errors_3(i: int = None):
    if i is None:
        i = random.choice([0, 1])
    if i:
        raise CatchError3()


@pytest.mark.parametrize('arg', [0, 1])
def test_catch_errors_3(arg: int):
    """
    Make sure CatchError3 is raised and flag is set to 1.
    """

    try:
        catch_errors_3(arg)
    except CatchError3:
        pass
    finally:
        flag = 1

    assert flag == 1
