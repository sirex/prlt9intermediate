from __future__ import annotations

import dataclasses
import datetime
from abc import ABC
from abc import abstractmethod
from copy import deepcopy
from enum import Enum
from typing import Iterator
from typing import Protocol
from typing import Tuple
from typing import Union
from typing import Any
from typing import List


def test_what_is_instance():
    class Person:
        name: str

        def __init__(self, name: str):
            self.name = name

    mantas = Person("Mantas")
    andrius = Person("Andrius")
    tomas = Person("Tomas")
    assert isinstance(mantas, Person)
    assert isinstance(andrius, Person)
    assert isinstance(tomas, Person)


def test_get_ints_from_list():

    def f(a: List[Any]):
        b = []
        for x in a:
            if isinstance(x, int):
                b.append(x)
        return b

    assert f([1, 'a', 2, 'b']) == [1, 2]


def test_what_is_instance_2():
    x: int = 1
    assert isinstance(x, int)


def test_polymorphism():

    @dataclasses.dataclass
    class Point:
        x: int
        y: int

        def func(self) -> int:
            return self.x * self.y

    @dataclasses.dataclass
    class Point3D:
        x: int
        y: int
        z: int

        def func(self) -> int:
            return self.x * self.y * self.z

    p2: Point = Point(1, 2)
    p3: Point3D = Point3D(1, 2, 3)

    def f(p: Union[Point, Point3D]) -> int:
        return p.func()

    r2: int = f(p2)
    r3: int = f(p3)


def test_polymorphism_protocols():

    @dataclasses.dataclass
    class Point:
        x: int
        y: int

        def func(self) -> int:
            return self.x * self.y

    @dataclasses.dataclass
    class Point3D:
        x: int
        y: int
        z: int

        def func(self) -> int:
            return self.x * self.y * self.z

    class SupportsFunc(Protocol):

        @abstractmethod
        def func(self) -> int: ...

    p2: Point = Point(1, 2)
    p3: Point3D = Point3D(1, 2, 3)

    def f(p: SupportsFunc) -> int:
        return p.func()

    r2: int = f(p2)
    r3: int = f(p3)


def test_inheritance():

    @dataclasses.dataclass
    class Point:
        x: int
        y: int

        def func(self) -> int:
            return self.x * self.y

    @dataclasses.dataclass
    class Point3D(Point):
        z: int

        def func(self) -> int:
            return super().func() * self.z

    def f(p: Point) -> int:
        return p.func()

    p2: Point = Point(1, 2)
    p3: Point3D = Point3D(1, 2, 3)

    r2: int = f(p2)
    r3: int = f(p3)


def test_inheritance_super():

    class Person:
        name: str
        age: int

        def __init__(self, name, age) -> None:
            self.name = name
            self.age = age

    class Employee(Person):
        rate: float
        num_of_hours: int

        def __init__(self, *args, rate, num_of_hours):
            super().__init__(*args)
            self.rate = rate
            self.num_of_hours = num_of_hours


def test_multiple_inheritance():

    class Person:
        name: str
        age: int

        def __init__(self, name, age) -> None:
            self.name = name
            self.age = age

        def get_full_name(self):
            return self.name

    class Employee(Person):
        rate: float
        num_of_hour: int

        def __init__(self, name, age, rate, num_of_hour):
            super().__init__(name, age)
            self.rate = rate
            self.num_of_hours = num_of_hour

    class Student(Person):
        scholarship: float

        def __init__(self, name, age, scholarship):
            super().__init__(name, age)
            self.scholarship = scholarship

        def show_finance(self):
            return self.scholarship

    class WorkingStudent(Employee, Student):
        def __init__(self, name, age, rate, num_of_hour, scholarship):
            Employee.__init__(self, name, age, rate, num_of_hour)
            Student.__init__(self, name, age, scholarship)

    mro = WorkingStudent.mro()

    employee = Employee("E1", 40, 50, 10)
    student = Student("S1", 20, 1000)
    ws = WorkingStudent("John", 42, 50, 100, 1000)

    ws.get_full_name()


def test_abstract_class():

    class Point(ABC):

        def __str__(self):
            return "Point"

        @abstractmethod
        def func(self) -> int: ...

    @dataclasses.dataclass()
    class Point2D(Point):
        x: int
        y: int

        def func(self) -> int:
            return self.x * self.y

    @dataclasses.dataclass()
    class Point3D(Point2D):
        z: int

        def func(self) -> int:
            return super().func() * self.z

    def f(p: Point) -> int:
        return p.func()

    p2: Point2D = Point2D(1, 2)
    p3: Point3D = Point3D(1, 2, 3)

    r2: int = f(p2)
    r3: int = f(p3)


def test_dataclass():

    class A:
        x: int
        y: int
        z: int

        def __init__(self, x, y, z):
            self.x = x
            self.y = y
            self.z = z

        def __repr__(self):
            return f'A(x={self.x!r}, y={self.y!r}, z={self.z!r})'

    @dataclasses.dataclass
    class B:
        x: int
        y: int
        z: int

    b = B(1, 2, 3)
    assert repr(b) == 'test_dataclass.<locals>.B(x=1, y=2, z=3)'


def test_copy():
    def f(z):
        return id(z)

    x = object()
    y = object()

    a = [x, y]
    b = a.copy()
    c = deepcopy(a)

    assert id(a) != id(b)       # b is shallow copy of a
    assert id(x) == id(a[0])
    assert id(x) == id(b[0])    # shallow copy does not copy a members
    assert id(x) != id(c[0])    # deepcopy also copies all members
    assert id(a) == f(a)        # functions pass mutable objects by referece


def test_copy_2():
    a = [1, [2, 3]]     # original list object
    b = a               # b is a reference to a
    c = a.copy()        # c is a shallow copy of a
    d = deepcopy(a)     # d is a deep copy of a

    b.append(4)
    assert a == [1, [2, 3], 4]      # <- changed
    assert b == [1, [2, 3], 4]      # <- changed
    assert c == [1, [2, 3]]
    assert d == [1, [2, 3]]

    a[1].append(5)
    assert a == [1, [2, 3, 5], 4]   # <- changed
    assert b == [1, [2, 3, 5], 4]   # <- changed
    assert c == [1, [2, 3, 5]]      # <- changed
    assert d == [1, [2, 3]]


def test_exercise_1():
    """Crate a family tree data model using classes.

    With this data model you should be able to:

    1. Name how two selected family members are related together.

    """

    class Role(Enum):
        FATHER = 'father'
        MOTHER = 'mother'
        CHILD = 'child'

    class Gender(Enum):
        MALE = 1
        FEMALE = 2

    @dataclasses.dataclass()
    class Person:
        name: str
        gender: Gender

    @dataclasses.dataclass()
    class Family:
        father: Person
        mother: Person
        children: List[Person]

    jonas = Person('Jonas', Gender.MALE)
    marija = Person('Marija', Gender.FEMALE)
    juozas = Person('Juozas', Gender.MALE)
    ona = Person('Ona', Gender.FEMALE)
    petras = Person('Petras', Gender.MALE)
    kaziukas = Person('Kazys', Gender.MALE)
    jonukas = Person("Jonas", Gender.MALE)

    family1 = Family(
        father=jonas,
        mother=marija,
        children=[
            juozas,
            ona,
        ]
    )

    family2 = Family(
        father=petras,
        mother=ona,
        children=[
            kaziukas,
            jonukas,
        ]
    )

    family_tree: List[Family] = [
        family1,
        family2,
    ]

    def find_families(
        tree: List[Family],
        person: Person,
    ) -> Iterator[Tuple[Family, Role]]:
        for family in tree:
            if family.father == person:
                yield family, Role.FATHER
            elif family.mother == person:
                yield family, Role.MOTHER
            else:
                for child in family.children:
                    if child == person:
                        yield family, Role.CHILD

    assert list(find_families(family_tree, ona)) == [
        (family1, Role.CHILD),
        (family2, Role.MOTHER),
    ]

    def kinship(
        tree: List[Family],
        person: Person,
        relative: Person,
    ) -> str:
        for family, role in find_families(tree, person):
            if role == Role.FATHER and relative == family.mother:
                return 'spouse'
            if role == Role.MOTHER and relative in family.children:
                return 'son'
            if role == Role.CHILD and relative == family.mother:
                return 'mother'

    assert kinship(family_tree, jonas, marija) == 'spouse'
    assert kinship(family_tree, marija, juozas) == 'son'
    assert kinship(family_tree, juozas, marija) == 'mother'

    # TODO: More difficult case, requires:
    #       1. Joining families and selecting grandparents.
    #       2. Checking if a relative is one of grandparents.
    assert kinship(family_tree, jonas, jonukas) == 'grandson'
    assert kinship(family_tree, jonukas, jonas) == 'grandfather'


def test_class_recap():

    class Gender(Enum):
        male = 'male'
        female = 'female'

    @dataclasses.dataclass
    class Person:
        name: str
        bday: datetime.date
        height: float   # cm
        weight: float   # kg
        gender: Gender

    mantas: Person = Person(
        name='Mantas',
        bday=datetime.date(2000, 1, 1),
        height=150,
        weight=70,
        gender=Gender.male,
    )

    assert mantas.name == 'Mantas'
    assert mantas.gender == Gender.male


def test_class_recap_no_dataclass_no_init():

    class Gender(Enum):
        male = 'male'
        female = 'female'

    class Person:
        name: str
        bday: datetime.date
        height: float   # cm
        weight: float   # kg
        gender: Gender

    mantas: Person = Person()
    mantas.name = 'Mantas'
    mantas.bday = datetime.date(2000, 1, 1)
    mantas.height = 150
    mantas.weight = 70
    mantas.gender = Gender.male

    assert mantas.name == 'Mantas'
    assert mantas.gender == Gender.male


def test_code_wars_5a03af9606d5b65ff7000009():

    @dataclasses.dataclass
    class User:
        name: str
        balance: int
        checking_account: bool

        def __str__(self):
            return f"{self.name} has {self.balance}"

        def withdraw(self, amount: int):
            self.balance -= amount
            return f"{self}."

        def check(self, user: User, amount: int):
            user.withdraw(amount)
            self.add_cash(amount)
            return f"{self} and {user}."

        def add_cash(self, amount: int):
            self.balance += amount
            return f"{self}."

    jeff = User('Jeff', 70, True)
    joe = User('Joe', 70, True)

    assert jeff.withdraw(2) == 'Jeff has 68.'
    assert joe.check(jeff, 50) == 'Joe has 120 and Jeff has 18.'
    assert jeff.check(joe, 80) == 'Jeff has 98 and Joe has 40.'
    assert jeff.add_cash(20) == 'Jeff has 118.'


def test_class_recap_inheritance():
    class Color(Enum):
        white = 'white'
        red = 'red'
        black = 'black'

    @dataclasses.dataclass
    class Skin:
        color: Color

    class Animal:
        weight: float   # kg
        skin: Skin
        bday: datetime.date

        def __init__(self, weight, skin, bday):
            self.weight = weight
            self.skin = skin
            self.bday = bday

        def age(self, date: datetime.date = None) -> int:
            date = date or datetime.date.today()
            return int((date - self.bday).total_seconds() / 60 / 60 / 24 / 365)

    class DomesticAnimal(Animal):
        name: str

        def __init__(self, name, *args, **kwargs):
            self.name = name
            super().__init__(*args, **kwargs)

    class Cat(DomesticAnimal):
        pass

    class Dog(DomesticAnimal):
        pass

    class Tiger(Animal):
        pass

    tom = Cat(
        name="Tom",
        weight=5,
        skin=Skin(color=Color.white),
        bday=datetime.date(2018, 1, 1),
    )

    assert tom.age(datetime.date(2021, 1, 1)) == 3


def test_code_wars_5a03af9606d5b65ff7000009_limit_classes():

    @dataclasses.dataclass
    class User:
        name: str
        balance: int
        checking_account: bool

    def state(*users: User) -> str:
        result = ' and '.join([
            f"{user.name} has {user.balance}"
            for user in users
        ])
        return f"{result}."

    def withdraw(user: User, amount: int) -> None:
        user.balance -= amount

    def check(to: User, from_: User, amount: int) -> None:
        withdraw(from_, amount)
        add_cash(to, amount)

    def add_cash(user: User, amount: int) -> None:
        user.balance += amount

    jeff = User('Jeff', 70, True)
    joe = User('Joe', 70, True)

    withdraw(jeff, 2)
    assert state(jeff) == 'Jeff has 68.'

    check(joe, jeff, 50)
    assert state(joe, jeff) == 'Joe has 120 and Jeff has 18.'

    check(jeff, joe, 80)
    assert state(jeff, joe) == 'Jeff has 98 and Joe has 40.'

    add_cash(jeff, 20)
    assert state(jeff) == 'Jeff has 118.'
