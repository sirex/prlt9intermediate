import csv
import re
from pathlib import Path
from typing import Iterator
from typing import List
from typing import Optional
from typing import Tuple

import pytest


def test_search():
    text = "Bandymas 1. Bandymas 2. Bandymas 42."
    match: Optional[re.Match] = re.search(r'\d+', text)
    assert match is not None
    assert isinstance(match, re.Match)


def test_findall():
    text = "Bandymas 1. Bandymas 2. Bandymas 42."
    result: List[str] = re.findall(r'\d+', text)
    assert result == ['1', '2', '42']


def test_findall_from_text_file(tmpdir: Path):
    file_path = tmpdir / 'tekstas.txt'
    file_path.write_text(
        "Bandymas 1. Bandymas 2. Bandymas 42.",
        encoding='utf-8',
    )

    # File can be specified manually.
    # file_path = r'C:\Users\Student\Desktop\memuarai.txt'
    with open(file_path) as f:
        text = f.read()

    result: List[str] = re.findall(r'\d+', text)
    assert result == ['1', '2', '42']

    # Write result to a CSV file
    csv_file_path: Path = tmpdir / 'data.csv'
    with open(csv_file_path, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['number'])
        for number in result:
            writer.writerow([number])

    # Check if results was stored in a csv file
    assert csv_file_path.read_text(encoding='utf-8').splitlines() == [
        'number',
        '1',
        '2',
        '42',
    ]


def test_search_no_match():
    text = "Bandymas 1. Bandymas 2. Bandymas 42."
    match: Optional[re.Match] = re.search(r'\d{4,}', text)
    assert match is None


def test_match():
    text = "Bandymas 1. Bandymas 2. Bandymas 42."
    match: Optional[re.Match] = re.match(r'Ba', text)
    assert match is not None
    assert isinstance(match, re.Match)


def test_match_no_match():
    text = "Bandymas 1. Bandymas 2. Bandymas 42."
    match: Optional[re.Match] = re.match(r'and', text)
    assert match is None


def test_fullmatch():
    text = "Bandymas"
    match: Optional[re.Match] = re.fullmatch(r'Band', text)
    assert match is None


def test_search_like_match():
    text = "Bandymas"
    match: Optional[re.Match] = re.search(r'^and', text)
    # Same as:                  re.match(r'and', text)
    assert match is None


def test_search_like_fullmatch():
    text = "Bandymas"
    match: Optional[re.Match] = re.search(r'^Band$', text)
    # Same as:                  re.fullmatch(r'Band', text)
    assert match is None


def test_finditer():
    text = "Bandymas 1. Bandymas 2. Bandymas 42."
    result: Iterator[re.Match] = re.finditer(r'\d+', text)
    result: List[re.Match] = list(result)
    assert len(result) == 3
    assert isinstance(result[0], re.Match)


def test_split():
    text = "Bandymas 1. Bandymas 2. Bandymas 42."
    result: List[str] = re.split(r'\.\s+|\.$', text)
    assert result == ['Bandymas 1', 'Bandymas 2', 'Bandymas 42', '']


def test_sub():
    text = "Bandymas 1. Bandymas 2. Bandymas 42."
    result: str = re.sub(r'\d+', 'N', text)
    assert result == "Bandymas N. Bandymas N. Bandymas N."


def test_sub_placeholders():
    text = "Bandymas 1. Bandymas 2. Bandymas 42."
    result: str = re.sub(r'(\d+)', r'[\1]', text)
    assert result == "Bandymas [1]. Bandymas [2]. Bandymas [42]."


def test_groups():
    text = "Bandymas 1. Bandymas 2. Bandymas 42."
    match: Optional[re.Match] = re.search(r'(\d+)\.', text)
    assert match is not None
    assert match.groups() == ('1',)
    assert match.group(1) == '1'


def test_groups_2():
    text = "Tomas S. (33), last seen in Vilnius"
    match: Optional[re.Match] = re.search(
        r'([A-Z][a-z]+ [A-Z]\.) \((\d+)\)',
        text,
    )
    assert match is not None
    assert match.groups() == ('Tomas S.', '33')
    assert match.group(0) == 'Tomas S. (33)'
    assert match.group(1) == 'Tomas S.'
    assert match.group(2) == '33'


def test_groups_3():
    text = "Tomas S. (33), last seen in Vilnius"
    result: List[Tuple[str, str]] = re.findall(
        r'([A-Z][a-z]+ [A-Z]\.) \((\d+)\)',
        text,
    )
    assert result == [('Tomas S.', '33')]


def test_groups_4():
    text = "Tomas S. (33), last seen in Vilnius"
    result: List[str] = re.findall(
        r'\((\d+)\)',
        text,
    )
    assert result == ['33']


# re.findall might be:
# 1. List[str] - if no groups are used
# 2. List[str] - if only one group is used
# 3. List[Tuple[str, ...]] - if more than one group is used


def test_task_1():
    text = "test43543lfdsfdsfl534543fdgl432fr"
    result = re.findall(r'\d+', text)
    assert result == [
        '43543',
        '534543',
        '432',
    ]


@pytest.mark.xfail
def test_task_1_2():
    text = "test43543lfdsfdsfl534543fdgl432fr"
    result = re.search(r'\d+', text)
    assert result == [
        '43543',
        '534543',
        '432',
    ]


def test_task_1a():
    text = "test43543lfdsfdsfl534543fdgl432fr"
    result = re.sub(r'\d+', '', text)
    assert result == "testlfdsfdsflfdglfr"


@pytest.mark.parametrize('number', [
    '48123456789',
    '+48123456789',
    '0048123456789',
])
def test_task_2(number: str):
    number_re = re.compile(r'(\+|00)?48\d{9}')
    assert number_re.fullmatch(number)


@pytest.mark.parametrize('number', [
    '48123456789',
    '+48123456789',
    '0048123456789',
])
def test_task_2_no_compile(number: str):
    assert re.fullmatch(r'(\+|00)?48\d{9}', number)


@pytest.mark.parametrize('number', [
    '48123456789',
    '+48123456789',
    '0048123456789',
])
def test_task_2_search(number: str):
    assert re.search(r'^(\+|00)?48\d{9}$', number)


@pytest.mark.parametrize('number', [
    '48123456789',
    '+48123456789',
    '0048123456789',
])
def test_task_2_search_compile(number: str):
    number_re = re.compile(r'(\+|00)?48\d{9}')
    assert number_re.search(number)
