Python intermediate
###################

How to get code
===============

::

    cd to/your/projects/dir
    git clone https://gitlab.com/sirex/prlt9intermediate.git
    cd prlt9intermediate


How to prepare project
======================

::

    python3 -m venv env  # make sure you use python 3.8 or later
    env/bin/pip install pytest



Command line cheat sheet
========================

============ ================== ============ ===================================
bash         ps                 cmd          description
============ ================== ============ ===================================
ls           ls                 dir          List files in current directory
cd           cd                 cd           Change active directory
which        Get-Command        where        Find executable file
============ ================== ============ ===================================


Pytest arguments
================

Additional arguments: ``-vvx --tb=short``

-vv
    double verbose

-x
    stop execution after first failure

--tb=short
    shorter tracebacks
