import contextlib
from pathlib import Path
from types import TracebackType
from typing import Iterator
from typing import Optional
from typing import TextIO
from typing import Type
from typing import Union


class FileManager:
    file: TextIO = None
    file_name: str

    def __init__(self, file_name: Union[str, Path]):
        self.file_name = file_name

    def __enter__(self) -> TextIO:
        self.file = open(self.file_name, 'w', encoding='utf-8')
        return self.file

    def __exit__(
        self,
        exc_type: Optional[Type[BaseException]],
        exc_value: Optional[BaseException],
        traceback: Optional[TracebackType],
    ) -> Optional[bool]:
        self.file.close()
        return None


@contextlib.contextmanager
def opener(file_name: Union[str, Path]) -> Iterator[TextIO]:
    file = open(file_name, 'wb', encoding='utf-8')
    yield file
    file.close()


def test_context_manager(tmpdir: Path):
    file_name = tmpdir / 'test.txt'
    with FileManager(file_name) as f:
        print('Bandymas.', file=f)
    assert file_name.read_text('utf-8') == 'Bandymas.\n'


def test_context_manager_2(tmpdir: Path):
    file_name = tmpdir / 'test.txt'
    with opener(file_name) as f:
        print('Bandymas.', file=f)
    assert file_name.read_text('utf-8') == 'Bandymas.\n'
