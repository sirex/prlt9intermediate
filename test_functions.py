def test_what_is_star_args():
    a = [1, 2, 3, 4]
    x, y, z, g = a
    assert a == [x, y, z, g]

    *args, b = a
    assert args == [1, 2, 3]
    assert b == 4


def test_what_is_star_args_in_function():
    def f(a, b, *args):
        return args

    assert f(1, 2) == ()
    assert f(1, 2, 3) == (3,)
    assert f(1, 2, 3, 4) == (3, 4)
    assert f(1, 2, 3, 4, 5) == (3, 4, 5)
