from typing import Dict


def test_dict():
    d: Dict[str, int] = {'a': 1, 'b': 2}
    assert list(d.items()) == [('a', 1), ('b', 2)]
    assert list(d.keys()) == ['a', 'b']
    assert list(d.values()) == [1, 2]