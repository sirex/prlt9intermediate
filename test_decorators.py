from typing import Any
from typing import Callable


def test_decorator():

    def translate(func: Callable) -> Callable:
        dictionary = {
            'Hello': 'Sveiki',
        }

        def wrapper():
            result = func()
            return dictionary[result]

        return wrapper

    @translate
    def speak():
        return "Hello"

    assert speak() == "Sveiki"


def test_decorator_with_args():

    def translate(lang: str) -> Callable:
        dictionary = {
            'lt': {
                'Hello': 'Sveiki',
            },
            'eo': {
                'Hello': 'Saluton',
            }
        }

        def decorator(func: Callable) -> Callable:

            def wrapper():
                result = func()
                return dictionary[lang][result]

            return wrapper

        return decorator

    @translate('lt')
    def speak():
        return "Hello"

    @translate('eo')
    def speak2():
        return "Hello"

    assert speak() == "Sveiki"
    assert speak2() == "Saluton"


def test_decorator_1():

    def decorate(func: Callable) -> Callable:
        return func

    @decorate
    def speak():
        return "Hello"

    assert speak() == "Hello"


def test_function_as_return_type():

    def add(a: int, b: int) -> int:
        return a + b

    def higher_order_function() -> Callable[[int, int], int]:
        return add

    f: Callable = higher_order_function()
    assert f(2, 2) == 4


def test_function_as_return_type_2():

    def higher_order_function() -> Callable[[int, int], int]:
        def add(a: int, b: int) -> int:
            return a + b
        return add

    f: Callable = higher_order_function()
    assert f(2, 2) == 4


def test_decorator_recap():

    def decorator(func: Callable) -> Callable:
        return func

    @decorator
    def add(a, b):
        return a + b

    assert add(2, 2) == 4


def test_decorator_recap_2():

    def decorator(func: Callable) -> Callable:
        return subtract

    def subtract(a, b):
        return a - b

    @decorator
    def add(a, b):
        return a + b

    assert add(2, 2) == 0


def test_decorator_recap_3():

    def decorator(func: Callable) -> Callable:

        def wrapper(*args):
            result = func(*args)
            return result * 2

        return wrapper

    @decorator
    def add(a, b):
        return a + b

    assert add(2, 2) == 8


def test_decorator_recap_4():

    def multiply(n: int) -> Callable:

        def decorator(func: Callable) -> Callable:

            def wrapper(*args):
                result = func(*args)
                return result * n

            return wrapper

        return decorator

    @multiply(3)
    def add(a, b):
        return a + b

    assert add(2, 2) == 12


def test_decorator_recap_5():

    class Multiply:
        n: int
        func: Callable

        def __init__(self, n: int):
            self.n = n

        def __call__(self, func: Callable) -> Callable:
            self.func = func
            return self.wrapper

        def wrapper(self, *args):
            result = self.func(*args)
            return result * self.n

    @Multiply(3)
    def add(a, b):
        return a + b

    assert add(2, 2) == 12
